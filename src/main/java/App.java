import gui.EnhancedView;

import javax.swing.*;

/**
 * Starting class for {@link EnhancedView GUI}.
 *
 * @author Karol Domański
 * @author Przemysław Dębski
 */
public class App
{
    public static void main(String args[])
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                try
                {
                    for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
                        if ("Nimbus".equals(info.getName()))
                        {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                }
                catch (Exception ignored)
                {}

                new EnhancedView().setVisible(true);
            }
        });
    }
}
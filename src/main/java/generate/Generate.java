package generate;

import singletons.DataManager;


/**
 * Created by HP on 2017-01-11.
 */
public class Generate {

    private DataManager data;
    private Input dataIn;

    public Zajecia[][][] schedule;

    public void init(){

       // System.out.println(dataIn.groupTab.length);

        schedule = new Zajecia[dataIn.groupTab.length][6][8];

        for(int i = 0; i < schedule.length; i++){
            for(int j = 0; j < 6; j++){
                for (int k = 0; k < 8; k++){

                    schedule[i][j][k] = new Zajecia();
                }
            }
        }


    }

    public void showSchedule(int group){
        for(int i = 0; i < 8 ;i++ ){
            for(int j = 0; j < 6; j++){
                //System.out.println("hahahah");
                //System.out.print(schedule[group][i][j].getName()+" "+schedule[group][i][j].isFlag()+"\t");
            }
            System.out.println("");
        }
    }

    public void generateSchedule() {


        for (int i = 0; i < dataIn.listClasses.length; i++) {

            if(dataIn.listClasses[i].isFlag()){
                int teacher = findTeacher(dataIn.listClasses[i].getTeacher());
                int group = findGroup(dataIn.listClasses[i].getGroup());

                for(int j = 0; j < dataIn.teacherTab[teacher].availableTab.length; j++){
                    for(int k = 0; k < dataIn.teacherTab[teacher].availableTab[j].length; k++){

                        if(dataIn.teacherTab[teacher].availableTab[j][k]){

                            if(dataIn.groupTab[group].availableTab[j][k]){

                                for(int z = 0; z < dataIn.roomTab.length; z++){

                                    if(dataIn.listClasses[i].getType().equals(dataIn.roomTab[z].getType())){

                                        if(dataIn.roomTab[z].availableTab[j][k]){

                                            dataIn.teacherTab[teacher].availableTab[j][k] = false;
                                            dataIn.groupTab[group].availableTab[j][k] = false;
                                            dataIn.roomTab[z].availableTab[j][k] = false;
                                            dataIn.listClasses[i].setFlag(false);

                                            dataIn.listClasses[i].setRoom(dataIn.roomTab[z].getId());

                                            schedule[dataIn.groupTab[group].getLocalID()][j][k] = dataIn.listClasses[i];

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    private int findGroup(String name){
        for (int i = 0; i < dataIn.groupTab.length; i++){
            if(name.equals(dataIn.groupTab[i].getName()) ){
                return i;
            }
        }
        return -1;
    }
    private int findTeacher(int id){
        for (int i = 0; i < dataIn.teacherTab.length; i++){
            if(id == dataIn.teacherTab[i].getId()){
                return i;
            }
        }
        return -1;
    }

    public Generate(DataManager dataInput){

        this.data = dataInput;
        this.dataIn = new Input(data);
        init();
}

}

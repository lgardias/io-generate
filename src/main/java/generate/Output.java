package generate;

import entity.util.EntityList;
import singletons.DataManager;

/**
 * Created by HP on 2017-01-11.
 */
public class Output {
    private DataManager dataManager;
    public Zajecia[][][] schedule;
    public EntityList<Zajecia> zajecia;
    public void findTeacherPlan(int idTeacher)
    {
        zajecia = new EntityList<Zajecia>();
        for(int i=0;i<schedule.length;i++)
        {
            for(int j=0;j<schedule[i].length;j++)
            {
                for(int k=0;k<schedule[i][j].length;k++)
                {
                    if(schedule[i][j][k].isFlag()) {
                        if (schedule[i][j][k].getTeacher() == idTeacher) {
                            zajecia.add(schedule[i][j][k]);

                        }
                    }
                }
            }
        }
        saveToJson(zajecia);
    }
    public void findGroupPlan(int idGroup)
    {
        zajecia = new EntityList<Zajecia>();
        for(int i=0;i<schedule[idGroup].length;i++)
        {
            for(int j=0;j<schedule[idGroup][i].length;j++)
            {
                if(schedule[idGroup][i][j].isFlag())
                {
                    zajecia.add(schedule[idGroup][i][j]);
                }
            }
        }
        saveToJson(zajecia);
    }
    public void findRoomPlan(int idroom)
    {
        zajecia = new EntityList<Zajecia>();
        for(int i=0;i<schedule.length;i++)
        {
            for(int j=0;j<schedule[i].length;j++)
            {
                for(int k=0;k<schedule[i][j].length;k++)
                {
                    if(schedule[i][j][k].isFlag()) {
                        if (schedule[i][j][k].getRoom() == idroom) {
                            zajecia.add(schedule[i][j][k]);
                        }
                    }
                }
            }
        }
        saveToJson(zajecia);
    }
    public void findClassesPlan(String name)
    {
        zajecia = new EntityList<Zajecia>();
        for(int i=0;i<schedule.length;i++)
        {
            for(int j=0;j<schedule[i].length;j++)
            {
                for(int k=0;k<schedule[i][j].length;k++)
                {
                    if(schedule[i][j][k].isFlag()) {
                        if (schedule[i][j][k].getName().equals(name)) {
                            zajecia.add(schedule[i][j][k]);
                        }
                    }
                }
            }
        }
        saveToJson(zajecia);
    }
    public Output(DataManager dataManager, Zajecia[][][] plan)
    {
        this.dataManager=dataManager;
        this.schedule=plan;
    }
    public void saveToJson(EntityList<Zajecia> result)
    {
        //zapis listy do pliku Jsona
    }
}
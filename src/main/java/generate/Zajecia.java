package generate;

/**
 * Created by HP on 2017-01-11.
 */
public class Zajecia {

    private boolean flag;
    private String name;
    private int teacher;
    private int room;
    private String type;
    private String group;

    public Zajecia(){
        this.name = "";
        this.teacher = 0;
        this.room = 0;
        this.type = "";
        this.flag = false;
        this.group = "";
    }

    public Zajecia(String name, int teacher, String type, String group){

        this.name = name;
        this.teacher = teacher;
        this.type = type;
        this.flag = true;
        this.room = 0;
        this.group = group;

    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getTeacher() {
        return teacher;
    }

    public void setTeacher(int teacher) {
        this.teacher = teacher;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}

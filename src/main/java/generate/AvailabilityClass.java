package generate;

/**
 * Created by HP on 2017-01-11.
 */
public class AvailabilityClass {
    private String name;
    private int id;
    private int localID;
    private String type;
    public boolean [][] availableTab = new boolean[6][8];

    public int getLocalID() {
        return localID;
    }

    public void setLocalID(int localID) {
        this.localID = localID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AvailabilityClass(){

        this.name = "null";
        this.id = -1;
        this.type = "";
        this.localID = -1;
        for (int i = 0; i < availableTab.length; i++){
            for(int j = 0; j < availableTab[i].length; j++){
                availableTab[i][j] = false;
            }
        }
    }


}

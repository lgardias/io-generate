package generate;

import singletons.DataManager;

import java.sql.Time;

/**
 * Created by Grzegorz Bizoń & Łukasz Gardiasz on 2017-01-08.
 */
public class Input {


    public AvailabilityClass[] teacherTab;
    public AvailabilityClass[] roomTab;
    public AvailabilityClass[] groupTab;
    public Zajecia[] listClasses;
    private DataManager dataManager;


    public Input(DataManager data) {
        this.dataManager = data;
        teacherTab = new AvailabilityClass[dataManager.lecturers.size()];

        for (int i = 0; i < dataManager.lecturers.size(); i++) {

            teacherTab[i] = new AvailabilityClass();
            teacherTab[i].setId(dataManager.lecturers.get(i).getId());
            teacherTab[i].setName(dataManager.lecturers.get(i).getFirstName() + " " + dataManager.lecturers.get(i).getLastName());

            for(int j = 0; j< dataManager.lecturers.get(i).getLecturerAvailabilityTimes().size(); j++){

                teacherTab[i].availableTab
                        [convertDay(dataManager.lecturers.get(i).getLecturerAvailabilityTimes().get(j).getAvailabilityTime().getWeekday().getName())]
                        [convertTime(dataManager.lecturers.get(i).getLecturerAvailabilityTimes().get(j).getAvailabilityTime().getStart())] = true;
            }

        }

        roomTab = new AvailabilityClass[dataManager.rooms.size()];

        for (int  i = 0; i <  dataManager.rooms.size(); i++){

            roomTab[i] = new AvailabilityClass();
            roomTab[i].setId(dataManager.rooms.get(i).getId());
            roomTab[i].setName(dataManager.rooms.get(i).getName());
            roomTab[i].setType(dataManager.rooms.get(i).getRoomType().getName());

            for(int j = 0; j < dataManager.rooms.get(i).getRoomAvailabilityTimes().size(); j++){

                roomTab[i].availableTab
                        [convertDay(dataManager.rooms.get(i).getRoomAvailabilityTimes().get(j).getAvailabilityTime().getWeekday().getName())]
                        [convertTime(dataManager.rooms.get(i).getRoomAvailabilityTimes().get(j).getAvailabilityTime().getStart())] = true;
            }

        }


        groupTab = new AvailabilityClass[dataManager.groups.size()];

        for (int  i = 0; i <  dataManager.groups.size(); i++){

            System.out.println("cos");
            groupTab[i] = new AvailabilityClass();
            groupTab[i].setId(dataManager.groups.get(i).getId());
            groupTab[i].setName(dataManager.groups.get(i).getName());
            groupTab[i].setType(dataManager.groups.get(i).getGroupType().getName());


        }

        for(int i = 0; i < groupTab.length; i++){

            groupTab[i].setLocalID(i);
        }

        listClasses = new Zajecia[dataManager.physicalClassesList.size()];
        for(int i = 0; i <  dataManager.physicalClassesList.size(); i++){

            listClasses[i] = new Zajecia(
                    dataManager.physicalClassesList.get(i).getClasses().getName(),
                    dataManager.physicalClassesList.get(i).getLecturer().getId(),
                    dataManager.physicalClassesList.get(i).getClasses().getClassesType().getName(),
                    dataManager.physicalClassesList.get(i).getGroup().getName()
            );
        }


    }

    public int convertTime(Time start){
        if(start.equals(new Time(8,0,0))){
            return 0;
        }else if(start.equals(new Time(9,45,0))){
            return 1;
        }else if(start.equals(new Time(11,30,0))){
            return 2;
        }else if(start.equals(new Time(13,15,0))){
            return 3;
        }else if(start.equals(new Time(15,00,0))){
            return 4;
        }else if(start.equals(new Time(16,45,0))){
            return 5;
        }else if(start.equals(new Time(18,30,0))){
            return 6;
        }else if(start.equals(new Time(20,15,0))){
            return 7;
        }else{
            return -1;
        }
    }

    public int convertDay(String day){
        if(day.equals("Poniedziałek")){
            return 0;
        }else if(day.equals("Wtorek")){
            return 1;
        }else if(day.equals("Środa")){
            return 2;
        }else if(day.equals("Czwartek")){
            return 3;
        }else if(day.equals("Piątek")){
            return 4;
        }else {
            return 5;
        }
    }

}

package gui.util;
import org.jdesktop.beansbinding.Validator;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Contains static methods responsible for displaying
 * various message dialogs.
 *
 * @author Karol Domański
 */
public final class MessageDialog
{
    private MessageDialog() {}

    /**
     * Displays an error dialog informing user about unsuccessful attempt
     * to save the data in the database.
     *
     * @param parentComponent   determines the <code>Frame</code>
     *                          in which the dialog is displayed;  if
     *                          <code>null</code>, or if the
     *                          <code>parentComponent</code> has no
     *                          <code>Frame</code>, a
     *                          default <code>Frame</code> is used
     */
    public static void showUnsuccessfulDataSaveDialog(Component parentComponent)
    {
        JOptionPane.showMessageDialog(
                parentComponent,
                "Wystąpił problem podczas zapisu danych do bazy.\n" +
                        "Jeśli nie jest to problem spowodowany brakiem połączenia\n" +
                        "z Internetem, proszę spróbować ponownie wczytać dane z bazy.",
                "Błąd zapisu danych",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Displays an error dialog informing user about unsuccessful attempt
     * to load the data from the database.
     *
     * @param parentComponent   determines the <code>Frame</code>
     *                          in which the dialog is displayed;  if
     *                          <code>null</code>, or if the
     *                          <code>parentComponent</code> has no
     *                          <code>Frame</code>, a
     *                          default <code>Frame</code> is used
     */
    public static void showUnsuccessfulDataReloadDialog(Component parentComponent)
    {
        JOptionPane.showMessageDialog(
                parentComponent,
                "Wystąpił problem podczas wczytywania danych z bazy.\n" +
                        "Proszę, upewnij się, że masz połączenie z Internetem.\n" +
                        "\nProgram będzie działał w trybie offline.",
                "Błąd odczytu danych",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Displays an error dialog informing user about invalid
     * input in a form with optional message about the error
     * that caused a problem.
     *
     * <p>If the specified error message is {@code null}, it will
     * be omitted.</p>
     *
     * @param parentComponent   determines the <code>Frame</code>
     *                          in which the dialog is displayed;  if
     *                          <code>null</code>, or if the
     *                          <code>parentComponent</code> has no
     *                          <code>Frame</code>, a
     *                          default <code>Frame</code> is used
     * @param errorMessage      the error that caused this dialog to show up
     * @see #showInvalidInputDialog(Component)
     */
    public static void showInvalidInputDialog(Component parentComponent, String errorMessage)
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("Formularz nie został poprawnie wypełniony.");

        if(errorMessage != null)
            builder.append("\n\nPrzyczyna: ").append(errorMessage);

        JOptionPane.showMessageDialog(
                parentComponent,
                builder.toString(),
                "Niepoprawnie wypełniony formularz",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Displays an error dialog informing user about invalid
     * input in a form.
     *
     * <p>This method invokes {@link #showInvalidInputDialog(Component, String)}
     * with the second parameter as {@code null}.</p>
     *
     * @param parentComponent   determines the <code>Frame</code>
     *                          in which the dialog is displayed;  if
     *                          <code>null</code>, or if the
     *                          <code>parentComponent</code> has no
     *                          <code>Frame</code>, a
     *                          default <code>Frame</code> is used
     * @see #showInvalidInputDialog(Component, String)
     */
    public static void showInvalidInputDialog(Component parentComponent)
    {
        showInvalidInputDialog(parentComponent, null);
    }

    /**
     * Displays a question dialog asking user whether they are
     * sure of deleting selected records from a component, for example
     * a JTable.
     *
     * <p>The dialog offers the following options:</p>
     * <ul>
     *     <li><i>Yes, I am sure</i> and</li>
     *     <li><i>No</i>.</li>
     * </ul>
     *
     * @param parentComponent   determines the <code>Frame</code>
     *                          in which the dialog is displayed;  if
     *                          <code>null</code>, or if the
     *                          <code>parentComponent</code> has no
     *                          <code>Frame</code>, a
     *                          default <code>Frame</code> is used
     * @return  0 if user is sure of their choice; any other value
     *          should be treated as negative answer
     */
    public static int showAreYouSureDialog(Component parentComponent)
    {
        Object[] options = {"Tak, na pewno", "Wait, what?"};

        return JOptionPane.showOptionDialog(parentComponent,
                "Czy na pewno usunąć zaznaczone rekordy?\n" +
                        "UWAGA: Obiekty będące w relacji z usuwanymi rekordami również mogą zostać utracone!",
                "Na pewno?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
    }

    /**
     * Displays an info message informing user that conflicting
     * {@link entity.util.EntityAvailabilityTime time intervals}
     * have not been assigned to a resource.
     *
     * @see entity.AvailabilityTime#isConflicting(java.util.List)
     * @param parentComponent   determines the <code>Frame</code>
     *                          in which the dialog is displayed;  if
     *                          <code>null</code>, or if the
     *                          <code>parentComponent</code> has no
     *                          <code>Frame</code>, a
     *                          default <code>Frame</code> is used
     */
    public static void showConflictingTimesDialog(Component parentComponent)
    {
        JOptionPane.showMessageDialog(
                parentComponent,
                "Nachodzące na siebie przedziały czasowe nie zostały dodane.",
                "Usunięto konflikty",
                JOptionPane.INFORMATION_MESSAGE);
    }
}


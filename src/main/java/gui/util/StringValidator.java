package gui.util;

import org.jdesktop.beansbinding.Validator;

/**
 * Validator used by JavaBean to check if the specified String
 * inserted by user is valid.
 *
 * <p>In this case, a String is considered valid if it's neither empty
 * nor it contains any numerical characters.</p>
 *
 * @author Karol Domański
 * @since 0.6
 */
public class StringValidator extends Validator<String>
{
    /**
     * @param s     String to be validated
     * @return      result with appropriate error code and description;
     *              {@code null} if there were no errors
     **/
    @Override
    public Result validate(String s)
    {
        if(s == null || s.isEmpty())
            return new Result(null, "To pole nie może być puste.");

        if(s.matches((".*\\d+.*")))
            return new Result(null, "Wprowadzony tekst nie może zawierać cyfr.");

        return null;
    }
}

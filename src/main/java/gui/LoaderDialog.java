/*
 * Created by JFormDesigner on Mon Dec 19 21:15:17 CET 2016
 */

package gui;

import gui.util.MessageDialog;
import net.miginfocom.swing.MigLayout;
import singletons.DataManager;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Dialog containing a fancy progress bar which informs about progress
 * of the process of loading data from the database.
 *
 * @author Karol Domański
 */
public class LoaderDialog extends JDialog implements PropertyChangeListener
{
    private static boolean isRunning = false;
    private ProgressUpdater updater = null;

    public LoaderDialog(Frame owner)
    {
        super(owner);
        initComponents();
    }

    public LoaderDialog(Dialog owner)
    {
        super(owner);
        initComponents();
    }

    /**
     * When this method is invoked, the following operations take place:
     * <ol>
     *     <li>The owner/parent of this JDialog is disabled (it won't
     *     respond to any input received).</li>
     *     <li>This JDialog is shown.</li>
     *     <li>A process of {@link DataManager#reloadEntities() reloading
     *     entities} from the database is started on a separate thread.</li>
     *     <li>A process of updating the {@link #progressBar} is started
     *     on a {@link #updater SwingWorker}.</li>
     * </ol>
     * <p>After loading is completed, parent is enabled and this dialog
     * is hidden automatically. It isn't {@link #dispose() disposed}, however.</p>
     * <p>While there may be multiple instances of this class across
     * the program, only <strong>one</strong> loading process is allowed
     * to take place. Invoking this method in one of the instances before
     * loading is finished will result in throwing {@link IllegalStateException}.</p>
     *
     * @throws IllegalStateException    if this method is invoked again before
     *                                  loading is finished;
     **/
    public void beginLoading()
    {
        if(isRunning)
            throw new IllegalStateException("There was already an unfinished process of reloading data from the database.");

        // block access to this method, just in case
        isRunning = true;

        // set a busy mouse cursor and lock the form
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        getOwner().setEnabled(false);

        // show this dialog
        setVisible(true);

        // begin loading entities from the database
        new Thread(() ->
        {
            if (!DataManager.getInstance().reloadEntities())
            {
                // show appropriate message dialog if reloading was unsuccessful
                MessageDialog.showUnsuccessfulDataReloadDialog(getParent());
            }
        }).start();

        // begin updating a progress bar
        updater = new ProgressUpdater(this);
        updater.execute();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        if (Objects.equals("progress", evt.getPropertyName()))
            progressBar.setValue((int) evt.getNewValue());
    }

    /**
     * Thread responsible for updating a visual state
     * of the {@link #progressBar progress bar}.
     */
    private final class ProgressUpdater extends SwingWorker<Void, Void>
    {
        private ProgressUpdater(PropertyChangeListener listener)
        {
            addPropertyChangeListener(listener);
        }

        @Override
        protected Void doInBackground()
        {
            // initialize progress property
            setProgress(0);

            // retrieve DataManager instance
            final DataManager dataManager = DataManager.getInstance();

            // thread's main loop
            while(!dataManager.isLoadingCompleted())
            {
                // keep updating progress every 0.25s
                setProgress((int) (dataManager.getLoadingProgress() * 100));
                try
                {
                    Thread.sleep(250);
                }
                catch (InterruptedException ignored) {}
            }

            return null;
        }

        @Override
        protected void done()
        {
            // reset a mouse cursor
            setCursor(null);

            // reset states of LoaderDialog
            isRunning = false;
            updater = null;
            progressBar.setValue(0);

            // hide this dialog and enable the parent
            setVisible(false);
            getOwner().setEnabled(true);
        }
    }

    @Override
    public void dispose()
    {
        // busy waiting for a task to finish
        if(updater != null)
            while(!updater.isDone())
                if(updater.isDone()) break;

        super.dispose();
    }

    private void initComponents()
    {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Revolver Ocelot
		progressBar = new JProgressBar();

		//======== this ========
		setAlwaysOnTop(true);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		setTitle("Wczytywanie danych");
		Container contentPane = getContentPane();
		contentPane.setLayout(new MigLayout(
			"hidemode 3",
			"[300,fill]",
			"[30]"));
		contentPane.add(progressBar, "cell 0 0,grow");
		pack();
		setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - Revolver Ocelot
	private JProgressBar progressBar;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

package entity;

import entity.util.AbstractModelObject;

import javax.persistence.*;

/**
 * Represents classes, student research group's classes,
 * internships (or whatever) led by a <strong>particular</strong>
 * {@link #lecturer} for a <strong>specific</strong> {@link #group}.
 *
 * @author Karol Domański
 */
@Entity
@Table(name = "physical_classes")
public class PhysicalClasses extends AbstractModelObject
{
    private int id;
    /** A person who leads/teaches these classes. */
    private Lecturer lecturer;
    /** Defines what kind of classes are these. */
    private Classes classes;
    /** A group these classes are meant for. */
    private Group group;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "lecturer_id", referencedColumnName = "id", nullable = false)
    public Lecturer getLecturer()
    {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer)
    {
        changeSupport.firePropertyChange("lecturer", this.lecturer, lecturer);
        this.lecturer = lecturer;
    }

    @ManyToOne
    @JoinColumn(name = "classes_id", referencedColumnName = "id", nullable = false)
    public Classes getClasses()
    {
        return classes;
    }

    public void setClasses(Classes classes)
    {
        changeSupport.firePropertyChange("classes", this.classes, classes);
        this.classes = classes;
    }

    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "id", nullable = false)
    public Group getGroup()
    {
        return group;
    }

    public void setGroup(Group group)
    {
        changeSupport.firePropertyChange("group", this.group, group);
        this.group = group;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhysicalClasses physicalClasses = (PhysicalClasses) o;

        if (id != physicalClasses.id) return false;
        if (lecturer != physicalClasses.lecturer) return false;
        if (group != physicalClasses.group) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = id;
        result += 31 * result + (lecturer != null ? lecturer.hashCode() : 0);
        result += 31 * result + (classes != null ? classes.hashCode() : 0);
        result += 31 * result + (group != null ? group.hashCode() : 0);
        return result;
    }
}

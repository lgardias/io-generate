package entity;

import entity.util.EntityAvailabilityTime;

import javax.persistence.*;

/**
 * Defines when a {@link #lecturer} assigned to this object
 * is available.
 *
 * @author Karol Domański
 */
@Entity
@Table(name = "lecturer_availability_time")
public class LecturerAvailabilityTime extends EntityAvailabilityTime
{
    private Lecturer lecturer;

    @Override
    public int hashCode()
    {
        return ((this.available) ? 1231 : 1237) * lecturer.hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LecturerAvailabilityTime that = (LecturerAvailabilityTime) o;

        if (available != that.available) return false;

        return true;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "lecturer_id", referencedColumnName = "id", nullable = false)
    public Lecturer getLecturer()
    {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer)
    {
        this.lecturer = lecturer;
    }
}

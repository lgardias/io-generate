package entity;

import entity.util.EntityType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents classes, student research group's classes,
 * internships or whatever.
 *
 * <p>This should be treated merely as a base for classes.
 * To define classes led by a particular {@link Lecturer}
 * for a specific {@link Group}, an entity class {@link PhysicalClasses}
 * should be used.</p>
 *
 * @author Karol Domański
 */
@Entity
@Table(name = "classes")
public class Classes extends EntityType
{
    /** Defines a type of these classes. */
    private ClassesType classesType;
    private List<PhysicalClasses> physicalClassesList = new ArrayList<>();

    public Classes(String name)
    {
        this.name = name;
    }

    public Classes()
    {}

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Classes classes = (Classes) o;

        if (id != classes.id) return false;
        if (name != null ? !name.equals(classes.name) : classes.name != null) return false;

        return true;
    }

    @ManyToOne
    @JoinColumn(name = "classes_type_id", referencedColumnName = "id", nullable = false)
    public ClassesType getClassesType()
    {
        return classesType;
    }

    public void setClassesType(ClassesType classesType)
    {
        changeSupport.firePropertyChange("classesType", this.classesType, classesType);
        this.classesType = classesType;
    }

    @OneToMany(mappedBy = "classes", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<PhysicalClasses> getPhysicalClassesList()
    {
        return physicalClassesList;
    }

    public void setPhysicalClassesList(List<PhysicalClasses> physicalClassesList)
    {
        changeSupport.firePropertyChange("physicalClassesList", this.physicalClassesList, physicalClassesList);
        this.physicalClassesList = physicalClassesList;
    }
}

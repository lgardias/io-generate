package entity;

import entity.util.AbstractModelObject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a single and unique "year" of faculty (in terms of group,
 * not time) or laboratory group.
 *
 * @author Karol Domański
 * @author Grzegorz Chromik
 */
@Entity
@Table(name = "`group`") // backticks are required because "group" is a reserved word in MySQL
public class Group extends AbstractModelObject
{
    private int id;
    private String name;
    /** Current amount of students in this group. */
    private Integer size;
    /** Defines a type of this group. */
    private GroupType groupType;
    private List<PhysicalClasses> physicalClassesList = new ArrayList<>();

    public Group(String name, Integer size)
    {
        this.name = name;
        this.size = size;
    }

    public Group()
    {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        changeSupport.firePropertyChange("name", this.name, name);
        this.name = name;
    }

    @Basic
    @Column(name = "size")
    public Integer getSize()
    {
        return size;
    }

    public void setSize(Integer size)
    {
        changeSupport.firePropertyChange("size", this.size, size);
        this.size = size;
    }

    @Override
    public int hashCode()
    {
        int result = id;
        result += 31 * result + (name != null ? name.hashCode() : 0);
        result += 31 * result + (size != null ? size.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (id != group.id) return false;
        if (name != null ? !name.equals(group.name) : group.name != null) return false;
        if (size != null ? !size.equals(group.size) : group.size != null) return false;

        return true;
    }

    @ManyToOne
    @JoinColumn(name = "group_type_id", referencedColumnName = "id", nullable = false)
    public GroupType getGroupType()
    {
        return groupType;
    }

    public void setGroupType(GroupType groupType)
    {
        changeSupport.firePropertyChange("groupType", this.groupType, groupType);
        this.groupType = groupType;
    }

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<PhysicalClasses> getPhysicalClassesList()
    {
        return physicalClassesList;
    }

    public void setPhysicalClassesList(List<PhysicalClasses> physicalClassesList)
    {
        changeSupport.firePropertyChange("physicalClassesList", this.physicalClassesList, physicalClassesList);
        this.physicalClassesList = physicalClassesList;
    }
}

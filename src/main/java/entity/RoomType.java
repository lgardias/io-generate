package entity;

import entity.util.EntityType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a type of a room, whether it is a classroom,
 * lecture room, an auditorium or whatever.
 *
 * @author Karol Domański
 * @author Grzegorz Chromik
 */
@Entity
@Table(name = "room_type")
public class RoomType extends EntityType
{
    /** Holds all the {@link Room rooms} this entity is assigned to. */
    private List<Room> rooms = new ArrayList<Room>();

    public RoomType(String name)
    {
        this.name = name;
    }

    public RoomType()
    {}

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomType roomType = (RoomType) o;

        if (id != roomType.id) return false;
        if (name != null ? !name.equals(roomType.name) : roomType.name != null) return false;

        return true;
    }

    @OneToMany(mappedBy = "roomType", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Room> getRooms()
    {
        return rooms;
    }

    public void setRooms(List<Room> rooms)
    {
        changeSupport.firePropertyChange("rooms", this.rooms, rooms);
        this.rooms = rooms;
    }
}

package entity;

import entity.util.EntityType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a weekday, therefore there should never be more than
 * 7 objects of this class (one per each weekday).
 *
 * @author Karol Domański
 */
@Entity
@Table(name = "weekday")
public class Weekday extends EntityType
{
    /**
     * Holds all the {@link AvailabilityTime availability times}
     * this weekday is assigned to.
     */
    private List<AvailabilityTime> availabilityTimes = new ArrayList<AvailabilityTime>();

    public Weekday(String name)
    {
        this.name = name;
    }

    public Weekday()
    {}

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Weekday weekday = (Weekday) o;

        if (id != weekday.id) return false;
        if (name != null ? !name.equals(weekday.name) : weekday.name != null) return false;

        return true;
    }

    @OneToMany(mappedBy = "weekday", cascade = CascadeType.ALL)
    public List<AvailabilityTime> getAvailabilityTimes()
    {
        return availabilityTimes;
    }

    public void setAvailabilityTimes(List<AvailabilityTime> availabilityTimes)
    {
        changeSupport.firePropertyChange("availabilityTimes", this.availabilityTimes, availabilityTimes);
        this.availabilityTimes = availabilityTimes;
    }
}

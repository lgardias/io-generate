package entity.util;

import javax.persistence.*;

/*
    Technically, all getter and setter methods in this class
    should be declared as final, but they can't be due to JPA
    standards and specifications.
 */

/**
 * An abstract mapped superclass that should be extended by all
 * entity classes which can be of exactly <strong>one</strong> type.
 *
 * @author Karol Domański
 */
@MappedSuperclass
public abstract class EntityType extends AbstractModelObject
{
    protected int id;
    protected String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        changeSupport.firePropertyChange("name", this.name, name);
        this.name = name;
    }

    @Override
    public final int hashCode()
    {
        int result = id;
        result += 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public abstract boolean equals(Object o);
}

package entity.util;

import entity.AvailabilityTime;

import javax.persistence.*;
import java.io.Serializable;

/*
    Technically, all getter and setter methods in this class
    should be declared as final, but they can't be due to JPA
    standards and specifications.
 */

/**
 * Abstract mapped superclass that should be extended
 * by all entity classes which define their own {@link AvailabilityTime
 * availability times}.
 *
 * @author Karol Domański
 */
@MappedSuperclass
public abstract class EntityAvailabilityTime implements Serializable
{
    /**
     * Whether this object is available/accessible at given
     * {@link #availabilityTime}.
     * */
    protected boolean available = true;

    /** Defines when this object is available/accessible. */
    private AvailabilityTime availabilityTime;

    @Basic
    @Column(name = "available")
    public boolean getAvailable()
    {
        return available;
    }

    public void setAvailable(boolean available)
    {
        this.available = available;
    }

    @Id
    @ManyToOne
    @OrderBy("weekday.id, start")
    @JoinColumn(name = "availability_time_id", referencedColumnName = "id", nullable = false)
    public AvailabilityTime getAvailabilityTime()
    {
        return availabilityTime;
    }

    public void setAvailabilityTime(AvailabilityTime availabilityTime)
    {
        this.availabilityTime = availabilityTime;
    }

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object o);
}

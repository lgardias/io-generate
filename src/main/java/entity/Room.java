package entity;

import entity.util.AbstractModelObject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a physical room of a specific {@link #roomType}.
 *
 * @author Karol Domański
 * @author Grzegorz Chromik
 */
@Entity
@Table(name = "room")
public class Room extends AbstractModelObject
{
    private int id;
    private String name;
    /**  How many students may enter this particular room. */
    private int capacity;
    /** Defines what type this room is of. */
    private RoomType roomType;
    private List<RoomAvailabilityTime> roomAvailabilityTimes = new ArrayList<RoomAvailabilityTime>();

    public Room(String name, int capacity, RoomType roomType)
    {
        this.name = name;
        this.capacity = capacity;
        this.roomType = roomType;
    }

    public Room()
    {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        changeSupport.firePropertyChange("name", this.name, name);
        this.name = name;
    }

    @Basic
    @Column(name = "capacity")
    public int getCapacity()
    {
        return capacity;
    }

    public void setCapacity(int capacity)
    {
        changeSupport.firePropertyChange("capacity", this.capacity, capacity);
        this.capacity = capacity;
    }

    @Override
    public int hashCode()
    {
        int result = id;
        result += 31 * result + (name != null ? name.hashCode() : 0);
        result += 31 * result + capacity;
        return result;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        if (id != room.id) return false;
        if (capacity != room.capacity) return false;
        if (name != null ? !name.equals(room.name) : room.name != null) return false;

        return true;
    }

    @ManyToOne
    @JoinColumn(name = "room_type_id", referencedColumnName = "id", nullable = false)
    public RoomType getRoomType()
    {
        return roomType;
    }

    public void setRoomType(RoomType roomType)
    {
        changeSupport.firePropertyChange("roomType", this.roomType, roomType);
        this.roomType = roomType;
    }

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<RoomAvailabilityTime> getRoomAvailabilityTimes()
    {
        return roomAvailabilityTimes;
    }

    public void setRoomAvailabilityTimes(List<RoomAvailabilityTime> roomAvailabilityTimes)
    {
        changeSupport.firePropertyChange("roomAvailabilityTimes", this.roomAvailabilityTimes, roomAvailabilityTimes);
        this.roomAvailabilityTimes = roomAvailabilityTimes;
    }
}

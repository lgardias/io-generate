package singletons;

import entity.*;
import entity.util.EntityList;
import org.hibernate.service.spi.ServiceException;

import javax.persistence.*;
import java.util.List;

/**
 * A singleton designed for database management. Its primary aim is to provide methods
 * capable of loading the entities from and persisting them to database.
 * <p>A communication with database is utilized through Java Persistence API with
 * Hibernate as JPA provider.</p>
 *
 * @author Karol Domański
 *
 * @see <a href="https://en.wikipedia.org/wiki/Java_Persistence_API">Java Persistence API</a>
 * @see <a href="https://en.wikipedia.org/wiki/Hibernate_(framework)">Hibernate</a>
 * @see <a href="https://dzone.com/articles/saving_detatched_entities">JPA - Saving detached entities</a>
 */
public class DataManager
{
    private static final DataManager instance = new DataManager();

    /** Total amount of time consuming operations performed during loading. */
    private final double denominator;
    /** Amount of time consuming operations that have already been processed. */
    private int loadingProgress;
    /** Whether loading is completed or not. */
    private boolean loadingCompleted;

    /**
     * Instance of interface used to interact with the entity manager
     * factory for the persistence unit.
     */
    private EntityManagerFactory entityManagerFactory = null;

    /** Instance of interface used to interact with the persistence context. */
    private EntityManager entityManager = null;
    
    // DATA LOADED FROM THE DATABASE
    public final EntityList<AvailabilityTime> availabilityTimes = new EntityList<AvailabilityTime>();
    public final EntityList<Classes> classesList = new EntityList<Classes>();
    public final EntityList<ClassesType> classesTypes = new EntityList<ClassesType>();
    public final EntityList<PhysicalClasses> physicalClassesList = new EntityList<>();
    public final EntityList<Group> groups = new EntityList<Group>();
    public final EntityList<GroupType> groupTypes = new EntityList<GroupType>();
    public final EntityList<Lecturer> lecturers = new EntityList<Lecturer>();
    public final EntityList<Room> rooms = new EntityList<Room>();
    public final EntityList<RoomType> roomTypes = new EntityList<RoomType>();
    public final EntityList<Weekday> weekdays = new EntityList<Weekday>(7);

    /** Array of all Entity Lists used to simplify a programmer's life. */
    private final EntityList[] allEntitiesLists = new EntityList[]
            {availabilityTimes, classesList, groups, lecturers, rooms, roomTypes, weekdays, classesTypes, groupTypes, physicalClassesList};

    /**
     * Returns the instance of this singleton.
     * @return  the only one instance of this singleton
     */
    public static DataManager getInstance()
    {
        return instance;
    }

    private DataManager()
    {
        denominator = allEntitiesLists.length + 3;
    }

    /**
     * Performs an operation of re-fetching all the entities from the database.
     *
     * <p>Since this method closes current {@link #entityManager Entity Manager}
     * and clears all the {@link EntityList Entity Lists}, any entities used
     * in a program after this method is called become detached and will have
     * any modifications ignored.</p>
     * <p>If this method is invoked on a separate thread, it becomes a primary
     * objective to make sure that none of the Entity Lists is accessed by other
     * threads before loading is finished. Accessing any of the Lists while the
     * process is running will result in unexpected behavior and exceptions.</p>
     * <p>To determine whether loading is completed or not, a method
     * {@link #isLoadingCompleted()} should be used.</p>
     *
     * @see #getLoadingProgress()
     * @see #isLoadingCompleted()
     *
     * @return  {@code true} if all entities were successfully fetched from the
     *          database and there were no errors or serious exceptions thrown
     *          during loading process; {@code false} otherwise
     */
    @SuppressWarnings("unchecked")
    public synchronized boolean reloadEntities()
    {
        // reset loading progress
        loadingCompleted = false;
        loadingProgress = 0;

        // free up resources
        shutDown();
        loadingProgress++;

        // create a new EntityManagerFactory
        try
        {
            entityManagerFactory = Persistence.createEntityManagerFactory("PersistenceUnit");
            loadingProgress++;
        }
        catch (ServiceException e)
        {
            e.printStackTrace();
            loadingCompleted = true;
            return false;
        }

        // create a new EntityManager
        entityManager = entityManagerFactory.createEntityManager();
        loadingProgress++;

        // clear all the EntityLists
        for (EntityList entities : allEntitiesLists)
            entities.clear();

        try
        {
            List<AvailabilityTime> availabilityTimeList = entityManager.createQuery("SELECT e FROM AvailabilityTime e order by e.weekday.id, e.start").getResultList();
            if(availabilityTimeList != null)
                availabilityTimes.addAll(availabilityTimeList);

            loadingProgress++;

            List<Classes> classesList = entityManager.createQuery("SELECT e FROM Classes e").getResultList();
            if(classesList != null)
                this.classesList.addAll(classesList);

            loadingProgress++;

            List<PhysicalClasses> physicalClassesList = entityManager.createQuery("SELECT e FROM PhysicalClasses e").getResultList();
            if(physicalClassesList != null)
                this.physicalClassesList.addAll(physicalClassesList);

            loadingProgress++;

            List<ClassesType> classesTypeList = entityManager.createQuery("SELECT e FROM ClassesType e").getResultList();
            if(classesTypeList != null)
                classesTypes.addAll(classesTypeList);

            loadingProgress++;

            List<Group> groupList = entityManager.createQuery("SELECT e FROM Group e").getResultList();
            if(groupList != null)
                groups.addAll(groupList);

            loadingProgress++;

            List<GroupType> groupTypeList = entityManager.createQuery("SELECT e FROM GroupType e").getResultList();
            if(groupTypeList != null)
                groupTypes.addAll(groupTypeList);

            loadingProgress++;

            List<Lecturer> lecturerList = entityManager.createQuery("SELECT e FROM Lecturer e").getResultList();
            if(lecturerList != null)
                lecturers.addAll(lecturerList);

            loadingProgress++;

            List<Room> roomList = entityManager.createQuery("SELECT e FROM Room e").getResultList();
            if(roomList != null)
                rooms.addAll(roomList);

            loadingProgress++;

            List<RoomType> roomTypeList = entityManager.createQuery("SELECT e FROM RoomType e").getResultList();
            if(roomTypeList != null)
                roomTypes.addAll(roomTypeList);

            loadingProgress++;

            List<Weekday> weekdayList = entityManager.createQuery("SELECT e FROM Weekday e").getResultList();
            if(weekdayList != null)
                weekdays.addAll(weekdayList);

            loadingProgress++;
        }
        catch (PersistenceException e)
        {
            e.printStackTrace();
            loadingCompleted = true;
            return false;
        }

        return loadingCompleted = true;
    }

    /**
     * Releases resources by closing instances of {@link #entityManagerFactory
     * EntityManagerFactory} and {@link #entityManager EntityManager}.
     *
     * <p>After this method has been invoked, any further changes to the entities
     * will no longer be tracked. The only way to reestablish the connection
     * to the database is by using {@link #reloadEntities()} method.</p>
     */
    public synchronized void shutDown()
    {
        // close current EntityManager
        if(entityManager != null && entityManager.isOpen())
            entityManager.close();

        // shut down current EntityManagerFactory
        if(entityManagerFactory != null && entityManagerFactory.isOpen())
            entityManagerFactory.close();
    }

    /**
     * Saves changes of the entities in the database.
     *
     * <p>When this method is invoked, all entities added/removed
     * from any of the {@link EntityList Entity Lists} of this singleton
     * will be persisted/deleted from the database. Any existing entities
     * which have been modified during runtime of a program will have
     * their corresponding properties updated in the database.</p>
     * <p>This method should never be invoked before {@link #reloadEntities()}
     * or after {@link #shutDown()}. Doing so will result in {@link IllegalStateException}.</p>
     *
     * @throws IllegalStateException    if {@link #entityManager Entity Manager}
     *                                  doesn't exist or is closed
     * @return  {@code true} if all entities have been successfully updated, persisted
     *          or removed and there were no errors or serious exceptions thrown
     *          during process; {@code false} otherwise
     */
    public synchronized boolean saveOrUpdateEntities() throws IllegalStateException
    {
        if(entityManager == null || !entityManager.isOpen())
            throw new IllegalStateException("EntityManager doesn't exist or was closed.");

        // get instance of transaction
        final EntityTransaction transaction = entityManager.getTransaction();

        try
        {
            transaction.begin();

            // save or delete objects from database
            for (EntityList list : allEntitiesLists)
                list.persistOrRemove(entityManager);
            
            transaction.commit();
        }
        catch (TransactionRequiredException | RollbackException | IllegalStateException e)
        {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }

    /**
     * Returns a value indicating current progress of data loading process.
     * This method might be useful if {@link #reloadEntities()} was invoked
     * on a separate thread.
     *
     * <p>Note that the value returned by this method should not be used
     * to determine whether loading is completed since it may not always be
     * possible for the progress to reach 1.</p>
     * <p>For checking whether loading process is finished or not, a method
     * {@link #isLoadingCompleted()} should be used.</p>
     *
     * @return  current progress of data loading process as value
     *          between 0 and 1
     */
    public double getLoadingProgress()
    {
        return (double) loadingProgress / denominator;
    }

    /**
     * Method used to determine whether a process of loading data from the
     * database is completed. This method might be useful if {@link #reloadEntities()}
     * was invoked on a separate thread.
     *
     * <p>Note that this method will return {@code true} even if loading operation
     * was unsuccessful (AKA {@code reloadEntities()} returned {@code false}).</p>
     *
     * @return  {@code true} if loading is completed;
     *          {@code false} otherwise
     */
    public boolean isLoadingCompleted()
    {
        return loadingCompleted;
    }
}
